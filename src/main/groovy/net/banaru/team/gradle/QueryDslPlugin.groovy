package net.banaru.team.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.tasks.Delete
import org.gradle.api.tasks.compile.JavaCompile

/**
 * QueryDsl Plugin
 *
 * @author banssolin
 */
class QueryDslPlugin implements Plugin<Project> {

    void apply(Project project) {
        if (!project.plugins.hasPlugin(JavaPlugin)) {
            project.plugins.apply(JavaPlugin)
        }

        def qdslDestDir = "${project.projectDir}/src/gen/qdsl"

        Configuration configuration = project.configurations.create('querydsl')
        configuration.visible = false

        project.dependencies {
            compile "com.querydsl:querydsl-jpa"
            querydsl "com.querydsl:querydsl-apt"
        }

        project.sourceSets.main.java.srcDir qdslDestDir
        project.task('generateQueryDSL', type: JavaCompile, group: 'queryDsl', description: 'Generates the QueryDSL query types') {
            source = project.sourceSets.main.java
            classpath = project.configurations.compile + project.configurations.querydsl
            destinationDir = project.file(qdslDestDir)
            options.compilerArgs += [
                "-proc:only",
                "-processor",
                "com.querydsl.apt.jpa.JPAAnnotationProcessor"
            ]
            options.encoding = "UTF-8"
        }

        project.task('cleanQueryDSL', type: Delete, group: 'queryDsl', description: 'delete the QueryDSL destination directory') { delete qdslDestDir }

        //        project.compileJava {
        //            dependsOn project.generateQueryDSL
        //        }
        //
        //        project.clean {
        //            delete qdslDestDir
        //        }
    }
}
