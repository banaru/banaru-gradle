package net.banaru.team.gradle

import net.banaru.team.gradle.constants.ENV
import org.gradle.api.Project
import org.gradle.api.plugins.GroovyPlugin
import org.gradle.api.tasks.bundling.Jar
import org.gradle.api.tasks.compile.GroovyCompile
import org.gradle.plugins.ide.eclipse.EclipsePlugin
import org.gradle.plugins.ide.eclipse.EclipseWtpPlugin
import org.gradle.plugins.ide.idea.IdeaPlugin

/**
 * @author banssolin
 */
class BuildUtils {

    //=====================================================
    // > Pass parameter: gradle build -Dbuild.env=STAGING
    // > -P: project properties, -D: system properties
    //=====================================================
    static ENV getBuildEnv() {
        def env = System.getProperty('build.env')
        return env == null ? ENV.LOCAL_IDE : ENV.valueOf(env)
    }

    static boolean isLocalIde() {
        return ENV.LOCAL_IDE == getBuildEnv()
    }

/**
 * Set jar Manifest (MANIFEST.MF) file format
 */
    def makeManifest = { project, manifest ->
        manifest.attributes["provider"] = "gradle"
        manifest.attributes["Created-By"] = "${System.getProperty('java.version')} " +
                "(${System.getProperty('java.specification.vendor')})"
        manifest.attributes["Implementation-Title"] = project.name
        manifest.attributes["Implementation-Version"] = project.version
        manifest.attributes["Implementation-Vendor"] = 'BANARU'
    }

/**
 * Javadoc configuration
 */
    def applyJavadoc = { Project project ->
        project.javadoc {
            source = project.sourceSets.main.allJava
            title = project.description
            failOnError = false
            options.locale = 'en_US'
            options.encoding = "UTF-8"
            options.charSet = "UTF-8"
            options.author = false
            options.version = false
            options.links("http://docs.oracle.com/javase/8/docs/api/")
            options.links("http://docs.spring.io/spring/docs/current/javadoc-api/")
            options.links("http://docs.spring.io/spring-boot/docs/current/api/")
            options.addStringOption('Xdoclint:none', '-quiet')
            options.addStringOption("sourcepath", "")
        }
    }

/**
 * Added javadocJar task
 */
    def applyJavadocJar = { Project project ->
        project.task('javadocJar', type: Jar, dependsOn: 'javadoc', group: 'build',
                description: 'Create a javadoc documentation jar') {
            classifier = 'javadoc'
            from project.javadoc.outputs.files
            //from javadoc.source
            makeManifest(project, manifest)
        }
        project.artifacts { archives project.javadocJar }
    }

/**
 * Added sourcesJar Task
 */
    def applySourceJar = { Project project ->
        project.task('sourcesJar', type: Jar, group: 'build', description: 'Create a sources jar') {
            classifier = 'sources'
            from project.sourceSets.main.allSource
            makeManifest(project, manifest)
        }
        project.artifacts { archives project.sourcesJar }
    }

/**
 * Apply Groovy Plugin
 */
    def applyGroovy = { Project project ->
        project.plugins.apply(GroovyPlugin)
        project.tasks.withType(GroovyCompile).each { it.groovyOptions.encoding = 'UTF-8' }
        project.dependencies {
            compile gradleApi()
            compile localGroovy()
        }
        project.task('groovydocJar', type: Jar, dependsOn: 'groovydoc', group: 'build', description: 'Create a ' +
                'groovydoc documentation jar') {
            classifier = 'groovydoc'
            from project.groovydoc.outputs.files
            makeManifest(project, manifest)
        }
        project.artifacts { archives project.groovydocJar }
    }

/**
 * setting IDE (Eclipse, Intellij IDEA)
 */
    def setIde = { project ->

        if (isLocalIde()) {
            /* IDE Setting */
            project.plugins.apply(IdeaPlugin)
            project.plugins.apply(EclipsePlugin)
            project.plugins.apply(EclipseWtpPlugin)

            // eclipse jdt java version
            project.eclipse.jdt {
                sourceCompatibility = project.sourceCompatibility
                targetCompatibility = project.targetCompatibility
            }

            // eclipse reference sources & javadoc
            project.eclipse.classpath {
                downloadSources = true
                downloadJavadoc = true
            }

            // idea reference sources & javadoc
            project.idea.module {
                inheritOutputDirs = false
                outputDir = project.file("${project.buildDir}/classes/main/")
                downloadJavadoc = true
                downloadSources = true
            }
        }
    }

/**
 *
 * running error at windows so check class path
 * reference http://tuhrig.de/gradles-bootrun-and-windows-command-length-limit/
 *
 */
    def makePathJar = { project ->
        project.task('pathJar', type: Jar) {
            dependsOn project.configurations.runtime
            appendix = 'path'
            doFirst {
                manifest {
                    attributes "Class-Path": project.configurations.runtime.files.collect {
                        project.uri(it).toString().replaceFirst(/file:\/+/, '/')
                    }.join(' ')
                }
            }
        }
    }

}
