package net.banaru.team.gradle


import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.BasePlugin
import org.gradle.api.plugins.WarPlugin
import org.gradle.api.tasks.Sync

/**
 * WebApp Plugin
 *
 * @author banssolin
 */
class WebAppPlugin implements Plugin<Project> {

    void apply(Project project) {
        project.plugins.apply(WarPlugin)

//        BuildUtils buildUtils = new BuildUtils()
//
//        if (BuildUtils.isLocalIde()) {
//            project.eclipse.wtp {
//                component {
//                    contextPath = '/'
//                    deployName = project.description
//                }
//                facet {
//                    facet name: 'jst.java', version: project.sourceCompatibility // java version
//                    facet name: 'jst.web', version: '3.1' // servlet version
//                    facet name: 'wst.jsdt.web', version: '1.0' // javascript version
//                }
//            }
//        }
//
//        // make web sources directory
//        project.webAppDir.mkdirs()
//
        // make classes war
        project.war {
            archiveBaseName.set("${project.name}")
            archiveVersion.set("${project.version}")
        }

        project.task('explodedWar', type: Sync) {
            group = BasePlugin.BUILD_GROUP
            into "${project.war.destinationDirectory.get()}/exploded/${project.war.archiveFileName.get()}"
            with project.war
        }
    }

}
