package net.banaru.team.gradle

import org.gradle.api.JavaVersion
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.compile.AbstractCompile

/**
 * @author banssolin
 */
class JavaPlugin implements Plugin<Project> {

    void apply(Project project) {
        project.plugins.apply(BuildPlugin)
        project.plugins.apply(org.gradle.api.plugins.JavaPlugin)
        BuildUtils buildUtils = new BuildUtils()

        // java compile version
        project.sourceCompatibility = JavaVersion.VERSION_15
        project.targetCompatibility = JavaVersion.VERSION_15
        project.tasks.withType(AbstractCompile).each {
            it.options.encoding = 'UTF-8'
            // it.options.compilerArgs << "-Xlint:unchecked" << "-Xlint:deprecation"
        }

        // add test dependency scope
        project.sourceSets {
            test.compileClasspath += project.configurations.compileOnly
            test.runtimeClasspath += project.configurations.compileOnly
        }

        buildUtils.applyJavadoc(project)
        buildUtils.setIde(project)

        // make classes jar
        project.jar {
            buildUtils.makeManifest(project, manifest)
        }

        // make sources directory
        project.sourceSets*.allSource.srcDirs.flatten()*.mkdirs()
    }

}
