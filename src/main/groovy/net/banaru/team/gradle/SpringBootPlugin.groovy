package net.banaru.team.gradle


import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.BasePlugin
import org.gradle.api.tasks.Sync

/**
 *  Spring Boot Plugin
 *
 * @author banssolin
 */
class SpringBootPlugin implements Plugin<Project> {

    void apply(Project project) {
        project.plugins.apply(WebAppPlugin)
        project.plugins.apply(org.springframework.boot.gradle.plugin.SpringBootPlugin)

        project.task('explodedBootWar', type: Sync) {
            group = BasePlugin.BUILD_GROUP
            dependsOn project.bootWar
            into "${project.war.destinationDirectory.get()}/exploded/${project.war.archiveFileName.get()}"
            from project.zipTree(project.bootWar.archiveFile)
        }

//        BuildUtils buildUtils = new BuildUtils()
//
//        // add test dependency scope
//        project.sourceSets {
//            test.compileClasspath += project.configurations.compileOnly
//            test.runtimeClasspath += project.configurations.compileOnly
//        }
//
//        project.processResources {
//            filesMatching('**/application.*') {
//                filter(ReplaceTokens, tokens: [name       : project.name,
//                                               description: project.description,
//                                               version    : project.version])
//            }
//        }
    }

}
