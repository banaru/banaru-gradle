package net.banaru.team.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.quality.Checkstyle
import org.gradle.api.plugins.quality.CheckstylePlugin
import org.gradle.api.plugins.quality.Pmd

//import org.gradle.api.plugins.quality.FindBugs
//import org.gradle.api.plugins.quality.FindBugsPlugin

import org.gradle.api.plugins.quality.PmdPlugin
import org.gradle.testing.jacoco.plugins.JacocoPlugin

/**
 * Analysis Plugin
 *
 * @author banssolin
 */
class AnalysisPlugin implements Plugin<Project> {

    void apply(Project project) {
        if (!project.plugins.hasPlugin(JavaPlugin)) {
            project.plugins.apply(JavaPlugin)
        }

        // analysis reports
        applyJacoco(project)
        applyCheckstyle(project)
        applyPmd(project)
        //applyFindBugs(project)
    }

    /**
     * Apply Coverage metrics for Java code by jacoco,
     * Reference http://www.eclemma.org/jacoco/
     */
    def applyJacoco = { Project project ->
        project.plugins.apply(JacocoPlugin)
        project.jacoco { toolVersion = '0.7.9' }
        project.jacocoTestReport {
            def jacocoExcludes = ['**/model/**/*']
            reports {
                xml.enabled = false
                html.enabled = true
                html.destination "${project.buildDir}/reports/jacoco"
            }
            classDirectories = project.fileTree(dir: "${project.buildDir}/classes/main", excludes: jacocoExcludes)
            sourceDirectories = project.files("${project.sourceSets.main.java}")
        }
        project.test.finalizedBy(project.tasks.jacocoTestReport)
    }

    /**
     * Code Analysis by CheckStyle,
     * Reference http://checkstyle.sourceforge.net/
     */
    def applyCheckstyle = { Project project ->
        project.plugins.apply(CheckstylePlugin)

        project.checkstyle {
            ignoreFailures = true
            toolVersion = '7.6.1'
            sourceSets = [project.sourceSets.main]
        }
        project.tasks.withType(Checkstyle) {
            include '**/*.java'
            exclude '**/model/**/*'
        }
    }

    /**
     * Code Analysis by PMD,
     * Reference https://pmd.github.io/
     */
    def applyPmd = { Project project ->
        project.plugins.apply(PmdPlugin)

        project.pmd {
            ignoreFailures = true
            toolVersion = '5.5.5'
            sourceSets = [project.sourceSets.main]
        }
        project.tasks.withType(Pmd) {
            include '**/*.java'
            exclude '**/model/**/*'
        }
    }

// Deprecated. FindBugs is unmaintained and does not support bytecode compiled for Java 9 and above.
//    /**
//     * Code Analysis by FindBugs
//     * Reference http://findbugs.sourceforge.net/
//     */
//    def applyFindBugs = { Project project ->
//        project.plugins.apply(FindBugsPlugin)
//        project.findbugs {
//            ignoreFailures = true
//            toolVersion = '3.0.1'
//            sourceSets = [project.sourceSets.main]
//            effort = "default"
//            reportLevel = "medium"
//        }
//        project.tasks.withType(FindBugs) {
//            include '**/*.java'
//            exclude '**/model/**/*'
//        }
//    }
}
