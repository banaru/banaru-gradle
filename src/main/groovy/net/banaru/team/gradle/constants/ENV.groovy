package net.banaru.team.gradle.constants

enum ENV {
    LOCAL_IDE, STAGING, PRODUCTION
}