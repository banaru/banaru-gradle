package net.banaru.team.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.Delete

/**
 * NodeJS Plugin
 *
 * @author banssolin
 */
class NodePlugin implements Plugin<Project> {

    void apply(Project project) {
        project.plugins.apply(BuildPlugin)
        project.plugins.apply(com.moowork.gradle.node.NodePlugin)

        // set node.js
        project.node {
            // Version of node to use.
            version = '6.9.2'

            // Version of npm to use.
            npmVersion = '4.0.3'

            // If true, it will download node using above parameters.
            // If false, it will try to use globally installed node.
            download = true

            // Set the work directory for unpacking node
            workDir = project.file("${project.rootDir}/.gradle/nodejs")

            // Set the work directory for NPM
            npmWorkDir = project.file("${project.rootDir}/.gradle/npm")

            // Set the work directory for Yarn
            yarnWorkDir = project.file("${project.rootDir}/.gradle/yarn")

            // Set the work directory where node_modules should be located
            nodeModulesDir = project.file("${project.projectDir}")

        }
        project.task('npmClean', type: Delete, group: 'node', description: 'Clean node modules') { delete "node_modules" }
    }

}
