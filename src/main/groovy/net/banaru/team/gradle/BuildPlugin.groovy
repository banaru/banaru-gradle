package net.banaru.team.gradle

import io.spring.gradle.dependencymanagement.DependencyManagementPlugin
import net.banaru.team.gradle.constants.ENV
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.invocation.Gradle
import org.gradle.tooling.UnsupportedVersionException
import org.gradle.util.VersionNumber

/**
 *
 * 1. Print logs.
 * 2. Check lowest gradle version.
 * 3. Apply spring "dependency-management-plugin".
 *
 * @author banssolin
 */
class BuildPlugin implements Plugin<Project> {

    static int PLUGINS_IN_PROJECT = 1

    static boolean isFirstPlugin() {
        PLUGINS_IN_PROJECT == 1
    }

    static void updatePluginCount() {
        PLUGINS_IN_PROJECT++
    }

    void apply(Project project) {
        //=====================================================
        // > Check lowest version
        //=====================================================
        Gradle gradle = project.gradle
        VersionNumber version = VersionNumber.parse(gradle.gradleVersion)
        VersionNumber requiredVersion = new VersionNumber(4, 4, 0, null)
        if (version.baseVersion < requiredVersion) {
            throw new UnsupportedVersionException(
                    "Your gradle version ($version) is too old. " +
                            "Plugin requires Gradle $requiredVersion+")
        }

        //=====================================================
        // > 子プロジェクトが1番目の場合のみ、ビルド時ログを出力する。
        // > 再ビルドの場合「PLUGINS_IN_PROJECT」が初期化されないため強制に初期化を行う。
        //=====================================================
        if (PLUGINS_IN_PROJECT > project.parent.childProjects.size()) {
            PLUGINS_IN_PROJECT = 1
        }

        if (firstPlugin) {
            def logLine = "+----------------------------------------------------------------+"

            project.logger.quiet "${logLine}"

            project.logger.quiet "  _                                                  Banaru Build Plugin"
            project.logger.quiet " | |__   __ _ _ __   __ _ _ __ _   _                 banssolin@gmail.com"
            project.logger.quiet " | '_ \\ / _` | '_ \\ / _` | '__| | | |       ${new Date()}"
            project.logger.quiet " | |_) | (_| | | | | (_| | |  | |_| |"
            project.logger.quiet " |_.__/ \\__,_|_| |_|\\__,_|_|  \\___,_|"

            // Banaru-Gradle Version
            Package pkg = this.class.getPackage()
            String thisPluginVersion = (pkg == null) ? "" : pkg.getImplementationVersion()
            if (thisPluginVersion) {
                project.logger.quiet " version : ${thisPluginVersion}"
            }
            project.logger.quiet "${logLine}"

            // Build Profile
            project.logger.quiet " Build Environement Profile : ${BuildUtils.getBuildEnv()}"

            // OS
            if (System.getProperty('os.name')) {
                project.logger.quiet " OS : ${System.getProperty('os.name')} (${System.getProperty('os.version')})"
            }
            // Java Version
            if (System.getProperty('java.version')) {
                project.logger.quiet " JRE : v${System.getProperty('java.version')}"
            }

            // JVM Name
            if (System.getProperty('java.vm.name')) {
                project.logger.quiet " JVM : ${System.getProperty('java.vm.name')}"
            }

            // Gradle Version
            project.logger.quiet " Gradle : v${project.gradle.gradleVersion}"
            project.logger.quiet " Guide : https://gitlab.com/banaru/banaru-gradle"
            project.logger.quiet "${logLine}"
        }
        updatePluginCount()

        //=====================================================
        // > io.spring.gradle:dependency-management-plugin 를 적용
        //=====================================================
        project.plugins.apply(DependencyManagementPlugin)

        // add g1center repositories
        //                project.repositories.ext.g1center = {
        //                    project.repositories.maven {
        //                        url project.getProperty("g1centerUrl")
        //                        if (project.getProperty("g1centerUserId") && project.getProperty("g1centerUserPw")) {
        //                            credentials {
        //                                username project.getProperty("g1centerUserId")
        //                                password project.getProperty("g1centerUserPw")
        //                            }
        //                        }
        //                    }
        //                }
    }

}

