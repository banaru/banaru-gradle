package net.banaru.team.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project

/**
 * Java Source Plugin
 *
 * @author banssolin
 */
class SourceJarPlugin implements Plugin<Project> {

    void apply(Project project) {
        if (!project.plugins.hasPlugin(JavaPlugin)) {
            project.plugins.apply(JavaPlugin)
        }
        BuildUtils buildUtils = new BuildUtils()

        // java source
        buildUtils.applySourceJar(project)
    }
}
