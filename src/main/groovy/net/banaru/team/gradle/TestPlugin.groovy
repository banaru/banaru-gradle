package net.banaru.team.gradle

import com.bmuschko.gradle.clover.CloverPlugin
import org.gradle.api.Plugin
import org.gradle.api.Project

import java.awt.*

/**
 * Test Module Plugin
 *
 * @author banssolin
 */
class TestPlugin implements Plugin<Project> {

    void apply(Project project) {

        if (!project.plugins.hasPlugin(org.gradle.api.plugins.JavaPlugin)) {
            project.plugins.apply(org.gradle.api.plugins.JavaPlugin)
        }
        project.plugins.apply(CloverPlugin)

        def generateCloverReport = false

        // if root project
        if (project.parent == null) {

            generateCloverReport = true

            // add task
            project.task('buildReport') {
                group = 'report'
                finalizedBy project.tasks.cloverAggregateReports
            }

            // print and popup test report on browser
            project.tasks.cloverAggregateReports.doLast {
                if (BuildUtils.isLocalIde()) {
                    project.logger.quiet(' see result:')
                    def reportPath = "${project.buildDir}".replace("\\", "/") +
                            "/reports/clover/html/dashboard.html"
                    project.logger.quiet(' ' + reportPath)
                    Desktop.desktop.browse "file:///${reportPath}".toURI()
                }
            }
        } else {
            project.dependencies {
                clover 'org.openclover:clover:4.3.1'
            }
        }

        // configure clover
        project.clover {
            testIncludes = ['**/*Test.java']
            report {
                html = generateCloverReport
            }
        }

        // disable junit html report
        project.test {
            reports.html.enabled = false
        }

        // Make directories
        // project.sourceSets.main.java.srcDirs.flatten()*.mkdirs()
    }

}
