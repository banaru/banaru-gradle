package net.banaru.team.gradle

import com.jfrog.bintray.gradle.BintrayPlugin
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.publish.maven.MavenPublication
import org.gradle.api.publish.maven.plugins.MavenPublishPlugin

/**
 * @author banssolin
 */
class PublishPlugin implements Plugin<Project> {

    void apply(Project project) {
        if (!project.plugins.hasPlugin(JavaPlugin)) {
            project.plugins.apply(JavaPlugin)
        }

        try {
            project.plugins.apply(MavenPublishPlugin)
            project.plugins.apply(BintrayPlugin)

            project.publishing {
                publications {
                    mavenPublication(MavenPublication) { from project.components.java }
                }
            }

            project.bintray {
                user = project.BANARU_USER
                key = project.BANARU_APIKEY

                pkg {
                    repo = project.BANARU_REPO
                    name = project.archivesBaseName
                    userOrg = project.BANARU_ORG // 조직에 업로드 하려면 작성, 개인 레포지토리면 코멘트 처리
                    licenses = ['Apache-2.0']
                    websiteUrl = 'https://banssolin@gitlab.com/banaru/banaru-lib.git'
                    issueTrackerUrl = 'https://gitlab.com/banaru/banaru-lib/issues'
                    vcsUrl = 'https://banssolin@gitlab.com/banaru/banaru-lib.git'
                    version {
                        name = project.version
                        released = new Date()
                        gpg {
                            sign = true
                            passphrase = project.BANARU_GPG_PASSWORD
                        }
                    }
                }
                publications = ['mavenPublication']
                configurations = ['archives']
            }
        } catch (Exception e) {
            project.logger.error("No exist Auth Info: BANARU")
            e.printStackTrace()
        }
    }

}