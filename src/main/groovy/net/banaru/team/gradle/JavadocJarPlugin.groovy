package net.banaru.team.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project

/**
 * JavaDoc Plugin
 *
 * @author banssolin
 */
class JavadocJarPlugin implements Plugin<Project> {

    void apply(Project project) {
        if (!project.plugins.hasPlugin(JavaPlugin)) {
            project.plugins.apply(JavaPlugin)
        }
        BuildUtils buildUtils = new BuildUtils()

        // java docs
        buildUtils.applyJavadocJar(project)
    }
}
